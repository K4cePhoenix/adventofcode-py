import numpy as np
import timeit

t0 = timeit.default_timer()

with open('./2019/input/aoc2019_02.txt', 'r') as f:
    data = f.read().split('\n')

def parse_data(dat):
    return list(map(int, dat[0].split(',')))

def part_1(data):
    data = parse_data(data)
    i = 0
    data[1] = 12
    data[2] = 2
    while True:
        if data[i] == 99:
            break
        elif data[i] == 1:
            ip1 = data[i+1]
            ip2 = data[i+2]
            ip3 = data[i+3]
            data[ip3] = data[ip1] + data[ip2]
            i += 4
        elif data[i] == 2:
            ip1 = data[i+1]
            ip2 = data[i+2]
            ip3 = data[i+3]
            data[ip3] = data[ip1] * data[ip2]
            i += 4

    return data[0]

def part_2(data):
    data0 = parse_data(data)
    for noun in range(0, 100):
        for verb in range(0, 100):
            data = data0.copy()
            i = 0
            data[1] = noun
            data[2] = verb
            while True:
                if data[i] == 99:
                    break
                elif data[i] == 1:
                    ip1 = data[i+1]
                    ip2 = data[i+2]
                    ip3 = data[i+3]
                    data[ip3] = data[ip1] + data[ip2]
                    i += 4
                elif data[i] == 2:
                    ip1 = data[i+1]
                    ip2 = data[i+2]
                    ip3 = data[i+3]
                    data[ip3] = data[ip1] * data[ip2]
                    i += 4
                else:
                    print("Error")
                    break
            if data[0] == 19690720:
                return 100 * noun + verb

print(part_1(data))
t1=timeit.default_timer()
print(part_2(data))
print(t1-t0, timeit.default_timer()-t0)
