import numpy as np
import timeit

t0 = timeit.default_timer()

with open('./2019/input/aoc2019_01.txt', 'r') as f:
    data = f.read().split('\n')

def parse_data(dat):
    return list(map(int, dat[:-1]))

def part_1(data):
    data = parse_data(data)
    fuel_req = list((x // 3) - 2 for x in data)
    return sum(fuel_req)

def part_2(data):
    data = parse_data(data)
    fuel_req = list((x // 3) - 2 for x in data)
    fuel_req_new = []
    for fr in fuel_req:
        tmp = fr // 3 - 2
        fr += tmp
        while True:
            tmp = tmp // 3 - 2
            if tmp <= 0:
                break
            fr += tmp
        fuel_req_new.append(fr)        

    return sum(fuel_req_new)

print(part_1(data))
t1=timeit.default_timer()
print(part_2(data))
print(t1-t0, timeit.default_timer()-t0)
